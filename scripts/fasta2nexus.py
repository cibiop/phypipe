#!/usr/bin/env python
# fasta2nexus.py
# Written by Nicolas D. Franco Sierra
# Universidad EAFIT, ago/2016
# It converts an alignment in FASTA format to NEXUS format
# Usage: fasta2nexus.py <input_alignment.fasta> <output_alignment.nexus>

import sys
from Bio import SeqIO
from Bio.Alphabet import IUPAC

input_file = sys.argv[1]
output_file = sys.argv[2]

input_handle = open(input_file, "rU")
output_handle = open(output_file, "w")

sequences = SeqIO.parse(input_handle, "fasta", alphabet=IUPAC.ambiguous_dna)
SeqIO.write(sequences, output_handle, "nexus")

output_handle.close()
input_handle.close()

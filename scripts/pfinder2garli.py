#!/usr/bin/env python 
# pfinder2garli.py
# Written by Nicolas D. Franco Sierra
# Universidad EAFIT, may/2015
# It receives a partitionfinder .cfg file and its best_scheme results obtained from analysis
# It outputs a text file containing garli block with partitions' information and the  corresponding garli.conf file
# Usage: pfinder2garli.py <partition_finder.cfg> <best_scheme.txt> <number_for_searchreps> <number_of_bootstrapreps>

import sys

input_cfg = str(sys.argv[1])
input_scheme = str(sys.argv[2])
searchreps = str(sys.argv[3])
bootstrapreps = str(sys.argv[4])

cfg_file = open(input_cfg, "r")
scheme_file = open(input_scheme, "r")

o = open("garli_pfinder.txt", "w")

conf1 = open("garli_ML.conf", "w")
conf2 = open("garli_BS.conf", "w")

w_flag = False

model_dict = {"GTR":"ratematrix = 6rate\nstatefrequencies = estimate\nratehetmodel = none\nnumratecats = 1\ninvariantsites = none\n",
              "GTR+I":"ratematrix = 6rate\nstatefrequencies = estimate\nratehetmodel = none\nnumratecats = 1\ninvariantsites = estimate\n",
              "GTR+G":"ratematrix = 6rate\nstatefrequencies = estimate\nratehetmodel = gamma\nnumratecats = 4\ninvariantsites = none\n",
              "GTR+I+G":"ratematrix = 6rate\nstatefrequencies = estimate\nratehetmodel = gamma\nnumratecats = 4\ninvariantsites = estimate\n",
              "SYM":"ratematrix = 6rate\nstatefrequencies = equal\nratehetmodel = none\nnumratecats = 1\ninvariantsites = none\n",
              "SYM+I":"ratematrix = 6rate\nstatefrequencies = equal\nratehetmodel = none\nnumratecats = 1\ninvariantsites = estimate\n",
              "SYM+G":"ratematrix = 6rate\nstatefrequencies = equal\nratehetmodel = gamma\nnumratecats = 4\ninvariantsites = none\n",
              "SYM+I+G":"ratematrix = 6rate\nstatefrequencies = equal\nratehetmodel = gamma\nnumratecats = 4\ninvariantsites = estimate\n",
              "HKY":"ratematrix = 2rate\nstatefrequencies = estimate\nratehetmodel = none\nnumratecats = 1\ninvariantsites = none\n", 
              "HKY+I":"ratematrix = 2rate\nstatefrequencies = estimate\nratehetmodel = none\nnumratecats = 1\ninvariantsites = estimate\n",
              "HKY+G":"ratematrix = 2rate\nstatefrequencies = estimate\nratehetmodel = gamma\nnumratecats = 4\ninvariantsites = none\n",
              "HKY+I+G":"ratematrix = 2rate\nstatefrequencies = estimate\nratehetmodel = gamma\nnumratecats = 4\ninvariantsites = estimate\n",
              "K80":"ratematrix = 2rate\nstatefrequencies = equal\nratehetmodel = none\nnumratecats = 1\ninvariantsites = none\n", 
              "K80+I":"ratematrix = 2rate\nstatefrequencies = equal\nratehetmodel = none\nnumratecats = 1\ninvariantsites = estimate\n",
              "K80+G":"ratematrix = 2rate\nstatefrequencies = equal\nratehetmodel = gamma\nnumratecats = 4\ninvariantsites = none\n",
              "K80+I+G":"ratematrix = 2rate\nstatefrequencies = equal\nratehetmodel = gamma\nnumratecats = 4\ninvariantsites = estimate\n",
              "F81":"ratematrix = 1rate\nstatefrequencies = estimate\nratehetmodel = none\nnumratecats = 1\ninvariantsites = none\n",
              "F81+I":"ratematrix = 1rate\nstatefrequencies = estimate\nratehetmodel = none\nnumratecats = 1\ninvariantsites = estimate\n",
              "F81+G":"ratematrix = 1rate\nstatefrequencies = estimate\nratehetmodel = gamma\nnumratecats = 4\ninvariantsites = none\n",
              "F81+I+G":"ratematrix = 1rate\nstatefrequencies = estimate\nratehetmodel = gamma\nnumratecats = 4\ninvariantsites = estimate\n",
              "JC":"ratematrix = 1rate\nstatefrequencies = equal\nratehetmodel = none\nnumratecats = 1\ninvariantsites = none\n",
              "JC+I":"ratematrix = 1rate\nstatefrequencies = equal\nratehetmodel = none\nnumratecats = 1\ninvariantsites = estimate\n",
              "JC+G":"ratematrix = 1rate\nstatefrequencies = equal\nratehetmodel = gamma\nnumratecats = 4\ninvariantsites = none\n",
              "JC+I+G":"ratematrix = 1rate\nstatefrequencies = equal\nratehetmodel = gamma\nnumratecats = 4\ninvariantsites = estimate\n",}


conf1.write("[general]\ndatafname = concat_w_partitions.nexus\nconstraintfile = none\nstreefname = stepwise\nattachmentspertaxon = 50\nofprefix = concat_ML\nrandseed = -1\navailablememory = 512\nlogevery = 10\nsaveevery = 100\nrefinestart = 1\noutputeachbettertopology = 0\noutputcurrentbesttopology = 0\nenforcetermconditions = 1\ngenthreshfortopoterm = 5000\nscorethreshforterm = 0.001\nsignificanttopochange = 0.01\noutputphyliptree = 0\noutputmostlyuselessfiles = 0\nwritecheckpoints = 0\nrestart = 0\noutgroup = 1\nresampleproportion = 1.0\ninferinternalstateprobs = 0\noutputsitelikelihoods = 0\noptimizeinputonly = 0\ncollapsebranches = 1\n\nsearchreps = "+ searchreps +"\nbootstrapreps = 0\nlinkmodels = 0\nsubsetspecificrates = 1\n\n")

conf2.write("[general]\ndatafname = concat_w_partitions.nexus\nconstraintfile = none\nstreefname = stepwise\nattachmentspertaxon = 50\nofprefix = concat_BS\nrandseed = -1\navailablememory = 512\nlogevery = 10\nsaveevery = 100\nrefinestart = 1\noutputeachbettertopology = 0\noutputcurrentbesttopology = 0\nenforcetermconditions = 1\ngenthreshfortopoterm = 5000\nscorethreshforterm = 0.001\nsignificanttopochange = 0.01\noutputphyliptree = 0\noutputmostlyuselessfiles = 0\nwritecheckpoints = 0\nrestart = 0\noutgroup = 1\nresampleproportion = 1.0\ninferinternalstateprobs = 0\noutputsitelikelihoods = 0\noptimizeinputonly = 0\ncollapsebranches = 1\n\nsearchreps = 1\nbootstrapreps = "+ bootstrapreps +"\nlinkmodels = 0\nsubsetspecificrates = 1\n\n")

part_dict = {}
count = 1

o.write("BEGIN SETS;\n\n")


for line in cfg_file:
    if w_flag:
        if line == "\n":
            w_flag = False
            break

        o.write("\t" + line)
        cur_line = line.split()
        part_dict[count] = str(cur_line[1])
        count += 1
       

    if str(line) == "[data_blocks]\n":
        w_flag = True


part_string = ""

w_flag = False
part_count = 0
for line in scheme_file:
    if w_flag:
        if line == "\n":
            w_flag = False
            break

        cur_line = line.split("|")
        model = cur_line[1].split()[0]
        parts = "".join(cur_line[2].split(",")).split()
        part_count += 1
        part_string += "chunk"+ str(part_count) + ":" + " ".join(parts) + ", "
        model_string = "\n[model" + str(part_count) +"]\ndatatype = nucleotide\n" + model_dict[model]
        conf1.write(model_string)
        conf2.write(model_string)

    if str(line[0:6]) == "Subset":
        w_flag = True

o.write("\n\tcharpartition bySites = " + part_string[0:-2] + ";\n\n")
o.write("END;\n")

end_conf = "\n\n[master]\nnindivs = 4\nholdover = 1\nselectionintensity = 0.5\nholdoverpenalty = 0\nstopgen = 5000000\nstoptime = 5000000\n\nstartoptprec = 0.5\nminoptprec = 0.01\nnumberofprecreductions = 5\ntreerejectionthreshold = 50.0\ntopoweight = 0.01\nmodweight = 0.002\nbrlenweight = 0.002\nrandnniweight = 0.1\nrandsprweight = 0.3\nlimsprweight =  0.6\nintervallength = 100\nintervalstostore = 5\nlimsprrange = 6\nmeanbrlenmuts = 5\ngammashapebrlen = 1000\ngammashapemodel = 1000\nuniqueswapbias = 0.1\ndistanceswapbias = 1.0"

conf1.write(end_conf)
conf2.write(end_conf)
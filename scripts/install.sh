#!/bin/bash
################################################################################
#
# Phypipe 0.1v - 05/13/2016
#
# Installation script to set the whole environment to run phypipe
#
# Written by Mateo Gomez-Zuluaga and Nicolas D. Franco-Sierra
#
# Universidad EAFIT
#
################################################################################

# Enable debug mode
#set -v
# MINT MORE DEB OS

# Check root access
if [[ $EUID -ne 0 ]]; then
    echo -e "\e[31mYou must be a root user\e[0m" 2>&1
    exit 1
fi

echo "Please enter the username for this configuration: "
read USER

HERE=`pwd`
BASHRC=/home/$USER/.bashrc
PYTHON="$(python2 -V 2>&1)"
PIP="$(pip -V 2> /dev/null)"
BPIPE=bpipe-0.9.9_beta_1.tar.gz
DNA2PEP=dna2pep-1.1.tgz
REVTRANS=revtrans-1.4.tgz
MAFFT=mafft-7.222-without-extensions-src.tgz
MRBAYES=mrbayes-3.2.6.tar.gz
GARLI=garli-2.01.tar.gz
RAXML=standard-RAxML-8.2.8.tar.gz
PARTITION=partitionfinder-phypipe.tar.gz
OS=`echo $(lsb_release -i) | sed 's/Distributor ID://' | tr -d " "`
RELEASE=`echo $(lsb_release -r) | sed 's/Release://' | tr -d " "`
DIRECTORY=/home/$USER/phypipe-run/venv
PIPELINE=$HERE/../pipeline

function help {
cat <<EOF

    install [OPTION]

    OPTION:
    all                      --  Install and configure the whole system.
    bpipe                    --  Install and configure bpipe.
    python                   --  Install python on the system.
    biopython                --  Install biopyton-1.66 library.
    dna2pep 1.1              --  Install and configure dna2pep.
    RevTrans 1.4             --  Install and configure RevTrans.
    MAFFT v7.222             --  Install and configure MAFFT without extensions. 
    partitionfinder 1.1.1    --  Install and configure PartitionFinder.
    MrBayes v3.2.6 x64       --  Install and configure MrBayes.
    garli 2.01               --  Install and configure Garli.
    SumTrees v 4.0.0         --  Install SumTrees package.
    RAxML 8.2.8              --  Install and configure RAxML.
    Remove Phypipe           --  Remove Phypipe
EOF

}

function all {
    
    echo -e "\n\e[32mCreating execution directory\n\e[0m"
    bpipe
    biopy
    dna2pep
    revtrans
    mafft
    sumtrees
    mrbayes
    garli
    raxml
    partitionfinder
    echo -e "\n\e[32mCopying examples to test Phypipe\n\e[0m"
    sudo -u $USER cp -r $HERE/examples $DIRECTORY 

}

function bpipe {

    echo -e "\n\e[34mInstall Bpipe and setup the environment variables\n\e[0m"
    rm -rf $HERE/apps/bpipe/*
    sudo -u $USER tar zvxf $HERE/apps/src/$BPIPE -C $HERE/apps/bpipe \
	 --strip=1 bpipe-0.9.9_beta_1

    if grep -q "## Bpipe" $BASHRC; then

	echo -e "\n\e[34mBASHRC is already updated\n\e[0m"
	
    else

	sudo -u $USER echo "## Bpipe" >> $BASHRC
	sudo -u $USER echo \
	     "export CLASSPATH=$CLASSPATH:$HERE/apps/bpipe/lib/bpipe.jar" >> \
	     $BASHRC 
	sudo -u $USER echo "export PATH=$HERE/apps/bpipe/bin:$PATH" >> $BASHRC
	echo -e "\n\e[32m BASHRC updated\n\e[0m"
	
    fi
    
}

function python {
    echo -e "\n\e[32mChecking if Python is installed\n\e[0m"

    if [[ $OS = "Fedora" ]]; then
	if [[ $RELEASE -ge "22" ]]; then
	    dnf install -y python python-devel redhat-rpm-config
	    echo -e "\n\e[34mPython installed\n\e[0m"
	elif [[ $RELEASE -le "21" ]]; then
	    yum install -y python
	    echo -e "\n\e[34mPython installed\n\e[0m"
	fi
    elif [[ $OS = "Ubuntu" ]]; then
	echo -e "\n\e[32mUpdating repolist...\n\e[0m"
	apt-get update -y > /dev/null && apt-get upgrade -y > /dev/null
	apt-get install -y python python-dev
	echo -e "\n\e[34mPython installed\n\e[0m"
	
    fi
    

    echo -e "\n\e[32mChecking if PIP is installed\n\e[0m"
    if echo $PIP | grep -q "pip"; then
	
	echo -e "\n\e[34mPIP is already installed\n\e[0m"
	
    else
	echo -e "\n\e[32mInstalling PIP package\n\e[0m"
	if [[ $OS = "Fedora" ]]; then
	    if [[ $RELEASE -ge "22" ]]; then
		dnf upgrade python-setuptools -y 
		dnf install -y python-pip python-wheel
		echo -e "\n\e[34mPIP installed\n\e[0m"
	    elif [[ $RELEASE -le "21" ]]; then
		yum upgrade python-setuptools -y 
		yum install -y python-pip python-wheel
		echo -e "\n\e[34mPIP installed\n\e[0m"
	    fi
	elif [[ $OS = "Ubuntu" ]]; then
	    echo -e "\n\e[32mUpadating repolist...\n\e[0m"
	    apt-get update -y > /dev/null && apt-get upgrade -y > /dev/null
	    apt-get install -y python-pip
	    echo -e "\n\e[34mPIP installed\n\e[0m"
	    
	fi
    fi

}

function biopy {

    python
    virtenv
    echo -e "\n\e[32mInstalling Biopython library and dependecies\n\e[0m"
    pip install numpy
    pip install biopython
    echo -e "\n\e[34mBiopython installed\n\e[0m"
}

function virtenv {

    echo -e "\n\e[32mInstalling virtualenv Python\n\e[0m"
    pip install virtualenv
    echo -e "\n\e[32mVirtualenv installed\n\e[0m"
    if [[ ! -d $DIRECTORY ]]; then
	echo -e "\n\e[32mCreating execution directory\n\e[0m"
	sudo -u $USER mkdir -p /home/$USER/phypipe-run
	cd /home/$USER/phypipe-run
	sudo -u $USER virtualenv venv
	source venv/bin/activate
    else
	cd /home/$USER/phypipe-run
	source venv/bin/activate
	
    fi     
}


function dna2pep {

    echo -e "\n\e[34mInstall dna2pep and setup the environment variables\n\e[0m"
    rm -rf $HERE/apps/dna2pep/*
    sudo -u $USER tar zvxf $HERE/apps/src/$DNA2PEP -C $HERE/apps/dna2pep \
	 --strip=1 dna2pep-1.1
    echo -e "\n\e[32mdna2pep installed\n\e[0m"

}

function revtrans {

    echo -e "\n\e[34mInstall RevTrans and setup the environment variables\n\e[0m"
    rm -rf $HERE/apps/revtrans/*
    sudo -u $USER tar zvxf $HERE/apps/src/$REVTRANS -C $HERE/apps/revtrans \
	 --strip=1 RevTrans-1.4
    echo -e "\n\e[32mRevTrans installed\n\e[0m"

}

function devtools {

    if [[ $OS = "Fedora" ]]; then
	if [[ $RELEASE -ge "22" ]]; then
	    dnf group install -y "Development Tools"
	    dnf install -y autoconf java-1.8.0-openjdk
	    echo -e "\n\e[34mDevelopment Tools installed\n\e[0m"
	elif [[ $RELEASE -le "21" ]]; then
	    yum group install -y "Development Tools"
	    yum install -y autoconf java-1.8.0-openjdk
	    echo -e "\n\e[34mDevelopment Tools installed\n\e[0m"
	fi
    elif [[ $OS = "Ubuntu" ]]; then
	apt-get update -y > /dev/null && apt-get upgrade -y > /dev/null
	apt-get install -y build-essential autoconf default-jre
	echo -e "\n\e[34mDevelopment Tools installed\n\e[0m"
	
    fi

}


function mafft {

    echo -e "\n\e[34mInstall MAFFT and setup the environment variables\n\e[0m"
    devtools
    rm -rf $HERE/apps/mafft/*
    sudo -u $USER tar zvxf $HERE/apps/src/$MAFFT -C $HERE/apps/src
    TOCHANGE="PREFIX = /usr/local"
    CHANGED="PREFIX = "$HERE"/apps/mafft"
    cd $HERE/apps/src/mafft-7.222-without-extensions/core
    sudo -u $USER sed -i "s~$TOCHANGE~$CHANGED~g" Makefile
    sudo -u $USER make
    sudo -u $USER make install
    rm -rf $HERE/apps/src/mafft-7.222-without-extensions
    cd $HERE
    echo -e "\n\e[32mMAFFT installed\n\e[0m"

}

function sumtrees {

    virtenv
    echo -e "\n\e[32mInstalling SumTrees dependencies\n\e[0m"
    pip install dendropy
    echo -e "\n\e[32mSumTrees library installed\n\e[0m"

}

function mrbayes {

    echo -e "\n\e[34mInstall MrBayes and setup the environment variables\n\e[0m"
    devtools
    rm -rf $HERE/apps/mrbayes/*
    sudo -u $USER tar zvxf $HERE/apps/src/$MRBAYES -C $HERE/apps/src
    cd $HERE/apps/src/mrbayes-3.2.6/src
    autoconf
    sudo -u $USER ./configure --prefix=$HERE/apps/mrbayes --enable-sse=no \
	 --with-beagle=no
    sudo -u $USER make 
    sudo -u $USER make install
    rm -rf $HERE/apps/src/mrbayes-3.2.6
    cd $HERE
    echo -e "\n\e[32mMrBayes installed\n\e[0m"

}

function garli {

    echo -e "\n\e[34mInstall Garli and setup the environment variables\n\e[0m"
    devtools
    rm -rf $HERE/apps/garli/*
    sudo -u $USER tar zvxf $HERE/apps/src/$GARLI -C $HERE/apps/src
    cd $HERE/apps/src/garli-2.01
    TOCHANGE="--prefix=\`pwd\` --with-ncl="
    CHANGED="--prefix="$HERE"/apps/garli --with-ncl="
    sudo -u $USER sed -i "s~$TOCHANGE~$CHANGED~g" build_garli.sh
    sudo -u $USER ./build_garli.sh
    rm -rf $HERE/apps/src/garli-2.01
    cd $HERE
    echo -e "\n\e[32mGarli installed\n\e[0m"
    
}

function raxml {

    echo -e "\n\e[34mInstall RAxML and setup the environment variables\n\e[0m"
    devtools
    rm -rf $HERE/apps/raxml/*
    sudo -u $USER tar zvxf $HERE/apps/src/$RAXML -C $HERE/apps/src
    cd $HERE/apps/src/standard-RAxML-8.2.8
    sudo -u $USER make -f Makefile.gcc
    sudo -u $USER mkdir -p $HERE/apps/raxml/bin
    sudo -u $USER cp raxmlHPC $HERE/apps/raxml/bin
    rm -rf $HERE/apps/src/standard-RAxML-8.2.8
    cd $HERE
    echo -e "\n\e[32mRAxML installed\n\e[0m"

}

function partitionfinder {

    echo -e "\n\e[34mInstall Partitionfinder\n\e[0m"
    rm -rf $HERE/apps/partitionfinder/*
    sudo -u $USER tar zvxf $HERE/apps/src/$PARTITION -C \
	 $HERE/apps/partitionfinder --strip=1 partitionfinder-phypipe
    sudo -u $USER ln -s $HERE/apps/raxml/bin/raxmlHPC \
	 $HERE/apps/partitionfinder/programs/raxml
    echo -e "\n\e[32mPartitionFinder installed\n\e[0m"
    
}

function remove {

    CLEANED=""
    TOCLEAN="## Bpipe"
    sudo -u $USER sed -i "s~$TOCLEAN~$CLEANED~g" $BASHRC
    TOCLEAN="export CLASSPATH=$CLASSPATH:$HERE/apps/bpipe/lib/bpipe.jar"
    sudo -u $USER sed -i "s~$TOCLEAN~$CLEANED~g" $BASHRC
    TOCLEAN="export PATH=$HERE/apps/bpipe/bin:$PATH"
    sudo -u $USER sed -i "s~$TOCLEAN~$CLEANED~g" $BASHRC
    rm -rf /home/$USER/phypipe-run
    rm -rf $HERE/apps/bpipe/*
    rm -rf $HERE/apps/dna2pep/*
    rm -rf $HERE/apps/garli/*
    rm -rf $HERE/apps/mafft/*
    rm -rf $HERE/apps/mrbayes/*
    rm -rf $HERE/apps/partitionfinder/* $HERE/apps/partitionfinder/.*
    rm -rf $HERE/apps/raxml/*
    rm -rf $HERE/apps/revtrans/*
    echo -e "\n\e[31mPhypipe removed\n\e[0m"
}

# Check if there is not arguments
if [[ $# == 0 ]]; then
    help
    exit 1;
fi

# Get arguments and execute them
for ARG in "$@"
do
    case $ARG in
	help)
	    help
	    #shift
	    ;;

	all)
	    all
	    ;;
	
	bpipe)
	    bpipe
	    ;;
	
	update)
	    update
	    ;;

	bashrc)
	    bashrc
	    ;;

	python)
	    python
	    ;;

	biopy)
	    biopy
	    ;;

	dna2pep)
	    dna2pep
	    ;;

	revtrans)
	    revtrans
	    ;;

	mafft)
	    mafft
	    ;;

	sumtrees)
	    sumtrees
	    ;;

	mrbayes)
	    mrbayes
	    ;;

	garli)
	    garli
	    ;;

	raxml)
	    raxml
	    ;;

	partitionfinder)
	    partitionfinder
	    ;;

	remove)
	    remove
	    exit 1
	    ;;
	
	*)
	    echo -e "\n   \e[31mUnrecognized option\n\e[0m"
	    help
	    exit 2
	    ;;
    esac
done

echo -e "\n\e[34mSetting-up Groovy dependencies path\n\e[0m"
cd $HERE
rm $PIPELINE/dependencies.groovy
sudo -u $USER echo "///////////////////////////////" >> $PIPELINE/dependencies.groovy
sudo -u $USER echo "" >> $PIPELINE/dependencies.groovy
sudo -u $USER echo "// paths to required software" >> $PIPELINE/dependencies.groovy
sudo -u $USER echo "" >> $PIPELINE/dependencies.groovy
sudo -u $USER echo "python="\"`which python`""\" >> $PIPELINE/dependencies.groovy
sudo -u $USER echo "dna2pep="\"$HERE"/apps/dna2pep/dna2pep.py"\" >> $PIPELINE/dependencies.groovy
sudo -u $USER echo "revtrans="\"$HERE"/apps/revtrans/revtrans.py"\" >> $PIPELINE/dependencies.groovy
sudo -u $USER echo "mafft="\"$HERE"/apps/mafft/bin/mafft"\" >> $PIPELINE/dependencies.groovy
sudo -u $USER echo "partitionfinder="\"$HERE"/apps/partitionfinder/PartitionFinder.py"\" >> $PIPELINE/dependencies.groovy
sudo -u $USER echo "mb=\""$HERE"/apps/mrbayes/bin/mb"\" >> $PIPELINE/dependencies.groovy
sudo -u $USER echo "garli="\"$HERE"/apps/garli/bin/Garli"\" >> $PIPELINE/dependencies.groovy
sudo -u $USER echo "sumtrees=\"/home/"$USER"/phypipe/venv/bin/sumtrees.py"\" >> $PIPELINE/dependencies.groovy
sudo -u $USER echo "raxmlHPC="\"$HERE"/apps/raxml/bin/raxmlHPC"\" >> $PIPELINE/dependencies.groovy
sudo -u $USER echo "dna2pep="\"$HERE"/apps/dna2pep/dna2pep.py"\" >> $PIPELINE/dependencies.groovy
sudo -u $USER echo "SCRIPTS="\"$HERE\" >> $PIPELINE/dependencies.groovy

chown $USER.$USER $PIPELINE/dependencies.groovy
echo -e "\n\e[32mGroovy dependencies created\n\e[0m"

echo -e "\n\e[31mRun the following command to finish the installation\n\e[0m"
echo "source $BASHRC"
exit 1

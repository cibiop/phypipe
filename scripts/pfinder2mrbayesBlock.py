#!/usr/bin/env python 
# pfinder2mrbayesBlock.py
# Written by Nicolas D. Franco Sierra
# Universidad EAFIT, may/2015
# It receives a partitionfinder .cfg file and its best_scheme results obtained from analysis
# It outputs in a text file the MrBayes block containing partition information, 
# ... selected models, and the excecution line for MCMC and consensus tree summary
# Usage: pfinder2mrbayesBlock.py <partition_finder.cfg> <best_scheme.txt> <number_of_generations_for_MCMC>

import sys

input_cfg = str(sys.argv[1])
input_scheme = str(sys.argv[2])
ngen = int(sys.argv[3])

cfg_file = open(input_cfg, "r")
scheme_file = open(input_scheme, "r")

o = open("Mrbayes_pfinder.txt", "w")

sample = ngen // 10000

w_flag = False

model_dict = {"GTR":["nst=6"], "GTR+I":["nst=6 rates=propinv"], "GTR+G":["nst=6 rates=gamma"],
              "GTR+I+G":["nst=6 rates=invgamma"], "SYM":["nst=6","statefreqpr=fixed(equal)"],
              "SYM+I":["nst=6 rates=propinv","statefreqpr=fixed(equal)"],
              "SYM+G":["nst=6 rates=gamma", "statefreqpr=fixed(equal)"],
              "SYM+I+G":["nst=6 rates=invgamma","statefreqpr=fixed(equal)"],
              "HKY":["nst=2"], "HKY+I":["nst=2 rates=propinv"], "HKY+G":["nst=2 rates=gamma"],
              "HKY+I+G":["nst=2 rates=invgamma"], "K80":["nst=2","statefreqpr=fixed(equal)"],
              "K80+I":["nst=2 rates=propinv","statefreqpr=fixed(equal)"],
              "K80+G":["nst=2 rates=gamma","statefreqpr=fixed(equal)"],
              "K80+I+G":["nst=2 rates=invgamma","statefreqpr=fixed(equal)"],
              "F81":["nst=1"], "F81+I":["nst=1 rates=propinv"], "F81+G":["nst=1 rates=gamma"],
              "F81+I+G":["nst=1 rates=invgamma"], "JC":["nst=1","statefreqpr=fixed(equal)"],
              "JC+I":["nst=1 rates=propinv", "statefreqpr=fixed(equal)"],
              "JC+G":["nst=1 rates=gamma", "statefreqpr=fixed(equal)"],
              "JC+I+G":["nst=1 rates=invgamma", "statefreqpr=fixed(equal)"]}

part_dict = {}
count = 1

o.write("BEGIN MRBAYES;\n\n")


for line in cfg_file:
    if w_flag:
        if line == "\n":
            w_flag = False
            break

        o.write("\t" + line)
        cur_line = line.split()
        part_dict[count] = str(cur_line[1])
        count += 1
       

    if str(line) == "[data_blocks]\n":
        w_flag = True


part_string = ', '.join(part_dict.values())

o.write("\n\tpartition favored = " + str(len(part_dict)) + ": " + part_string + ";\n\n\tset partition = favored;\n\n")

w_flag = False

for line in scheme_file:
    if w_flag:
        if line == "\n":
            w_flag = False
            break

        cur_line = line.split("|")
        model = cur_line[1].split()[0]
        parts = "".join(cur_line[2].split(",")).split()
        part_lst = []
        for part in parts:
            for part_id, part_st in part_dict.iteritems():
                if part_st == part:
                    part_lst.append(str(part_id))
        o.write("\tlset applyto=("+",".join(part_lst) + ") " + model_dict[model][0] + ";\n")
        if len(model_dict[model]) != 1:
            o.write("\tprset applyto=("+",".join(part_lst) + ") " + model_dict[model][1] + ";\n")

    if str(line[0:6]) == "Subset":
        w_flag = True

o.write("\n\tmcmc ngen="+str(ngen)+" relburnin=yes "+"samplefreq="+str(sample)+" burninfrac=0.35;\n")
o.write("\tsumt showtreeprobs=yes;\n")
o.write("END;\n")
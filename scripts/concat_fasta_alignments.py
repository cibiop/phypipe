#!/usr/bin/env python
# concat_fasta_alignments.py
# Written by Nicolas D. Franco-Sierra
# Universidad EAFIT, march/2016
# It receives several alignments (more than one) in FASTA format and outputs a concatenated single file
# If there are missing taxa in any file, it will be completed with gaps containing proper number of dashes
# Usage: concat_fasta_align.py alignment_1.fasta alignment_2.fasta alignment... output_file.fasta

from Bio.Seq import Seq
from Bio import SeqIO
from Bio import SeqRecord
import sys


def seqrecord_iter(fasta):
    return SeqIO.parse(fasta, 'fasta')

def check_identity(name):
    if type(name) is list:
        iterator = name
    else:
        iterator = seqrecord_iter(name)
    return iterator

def concat_pair(fasta_1, fasta_2):
    records = dict()
    for record in check_identity(fasta_1):
        record_id = record.id    
        records[record_id] = record.seq
    num = len(records.values()[0])
    record_list = []
    id_list = []
    for record in check_identity(fasta_2):
        record_id = record.id
        id_list.append(record_id)
        if record_id in records:
            r = record
            r.seq = Seq(str(records[record_id])) + record.seq
        else:
            fillerseq = Seq("-" * num)
            cur_seq = fillerseq + record.seq
            r = SeqRecord.SeqRecord(cur_seq, id=record_id, description=record_id, name=record_id)
        record_list.append(r)
    num2 = len((x for x in check_identity(fasta_2)).next().seq)
    extra_elems = set(records.keys()) - set(id_list)
    if len(extra_elems) != 0:
        for elem in extra_elems:
            elem = str(elem)
            prev_seq = Seq(str(records[elem])) + Seq("-" * num2)
            r = SeqRecord.SeqRecord(prev_seq, id=elem, description=elem, name=elem)
            record_list.append(r)
    return record_list


# get parameters
fasta_list = list(sys.argv)[1:-1]
output_file = str(sys.argv[-1])

output = open(output_file, 'w')

if len(fasta_list) == 1: # if a single file is provided no concat is needed, output the input
    last_pair = SeqIO.parse(fasta_list[0], 'fasta') 
else:
    fasta_1 = fasta_list[0]
    fasta_2 = fasta_list[1]
    last_pair = concat_pair(fasta_1, fasta_2) # concat first pair


if len(fasta_list) > 2: # if isn't just one pair, concat everything!!!!
    for fasta in fasta_list[2:]:
        last_pair = concat_pair(last_pair, fasta)


SeqIO.write(last_pair, output, 'fasta') # write to file

#!/usr/bin/env python 
# pfinder2raxml.py
# Written by Nicolas D. Franco Sierra
# Universidad EAFIT, march/2016
# It receives as input a best_scheme results file from partitionfinder analysis
# It outputs a text file containing the raxml block with partition information
# Usage: pfinder2raxml.py <best_scheme.txt>

import sys

input_scheme = str(sys.argv[1])
scheme_file = open(input_scheme, "r")

o = open("parts.txt", "w")

w_flag = False
for line in scheme_file:
    if w_flag:
        o.write(line)
    if str(line) == "RaxML-style partition definitions\n":
        w_flag = True
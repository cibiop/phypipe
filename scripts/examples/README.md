# Example datasets included with PhyPipe

Here we provide two datasets obtained from the flollowing studies:

* Díaz-Nieto JF, Jansa SA. & Voss RS. 2016. Phylogenetic relationships of Chacodelphys (Marsupialia: Didelphidae: Didelphinae) based on “ancient” DNA sequences. Journal of Mammalogy. DOI: [10.1093/jmammal/gyv197](https://doi.org/10.1093/jmammal/gyv197).

* Díaz-Nieto JF, Jansa SA. & Voss RS. 2016. DNA sequencing reveals unexpected Recent diversity and an ancient dichotomy in the American marsupial genus Marmosops (Didelphidae: Thylamyini). Zoological Journal of the Linnean Society. 176: 914–940 DOI: [10.1111/zoj.12343](https://doi.org/10.1111/zoj.12343).

We also provide example run parameters file in **params.txt**. This file can be copied to working folders and edited in order to fully configurate the pipeline options. See usermanual for further details. 
 

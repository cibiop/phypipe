# Parameters for PartitionFinder execution
-p branchlengths=1 # |1: linked  | 2: unlinked 
-p method=1 # | 1: mrbayes | 2: raxml | 3: beast | 4: all
-p modelselect=1 # | 1: BIC | 2:AIC | 3:AICc
-p searchselect=1 # | 1: greedy | 2: all | 3: rcluster | 4: hcluster
# Method choice for phylogenetic analysis 
-p phylo_method=raxml # (valid options: mrbayes | garli | raxml)
# Parameters for MrBayes execution
-p ngen=500000 # Number of generations for MrBayes MCMC
# Parameters for GARLI execution
-p searchreps=5 # number of replicates for ML search
-p bootstrapreps=1000 # number of bootstrap replicates
# Parameters for RAxML execution
-p raxmlreps=1000 # Number of bootstrap replicates for RAxML analysis
-p raxmlmodel=GTRGAMMA # Model for RAxML execution

#!/usr/bin/env python
# create_cfg.py
# Written by Nicolas D. Franco Sierra
# Universidad EAFIT, may/2015 updated on feb/2016
# It receives individual FASTA alignments placed in the same directory. Use <*.fasta>
# It outputs a .cfg configuration file for PartitionFinder containing preliminary partitions
# It generates a partition per locus, except when file is flagged as protein coding generating 3 partitions instead (1 per codon position)

# Configuration parameters must be specified in execution line. Check equivalencies between partitionfinder parameters and script's options on 'option dicts' section

# Usage create_cfg.py <output_file> *.fasta <branchlengths_code> <methodid_code> <modelselect_code> <searchselect_code>


# formating notes for input files: avoid names containing any dot but extension's dot.
# _cds tag must be immediately before ".fasta" extension

import sys
from Bio import SeqIO

def length_counter(fasta):
    first_record = SeqIO.parse(open(fasta, "rU"), "fasta").next()
    return len(first_record.seq)


# input parameters
cfg_output = sys.argv[1]
genes = sys.argv[2:-4]
branchlengths_id = sys.argv[-4]
method_id = sys.argv[-3]
model_select_id = sys.argv[-2]
search_select_id = sys.argv[-1]


# option dicts
branchlengths_dict = {"1":"linked","2":"unlinked"}
method_dict = {"1":"mrbayes","2":"raxml","3":"beast","4":"all"} # option 3 is not planned to be used in the pipeline
model_select_dict = {"1":"BIC","2":"AIC","3":"AICc"}
search_select_dict = {"1":"greedy","2":"all","3":"rcluster","4":"hcluster"}

# defining parameters
branchlengths = branchlengths_dict[branchlengths_id]
method = method_dict[method_id]
model_select = model_select_dict[model_select_id]
search_select = search_select_dict[search_select_id]

# creating and writing cfg
o = open(cfg_output, "w")

o.write("alignment = concat_matrix.phy;\n\nbranchlengths = "+ branchlengths +";\n\nmodels = "+ method +";\n\nmodel_selection = " + model_select +";\n\n\n[data_blocks]\n")

cumulative = 0

for gene in genes:
    tag = gene.split("_")[-1].split(".")[0]
    gene_name = gene.split("/")[-1].split(".")[0]
    gene_len = length_counter(gene)
    if tag[0:3]== "cds":
        gene_status = True
    else:
        gene_status = False
    ini = cumulative + 1
    cumulative += gene_len
    if gene_status:
        o.write("charset " + gene_name + "_pos1 = " + str(ini) + "-" + str(cumulative) + r"\3;"+"\n")
        o.write("charset " + gene_name + "_pos2 = " + str(ini + 1) + "-" + str(cumulative) + r"\3;"+"\n")
        o.write("charset " + gene_name + "_pos3 = " + str(ini + 2) + "-" + str(cumulative) + r"\3;"+"\n")
    else:
        o.write("charset " + gene_name + " = " + str(ini) + "-" + str(cumulative) + ";\n")

o.write("\n\n[schemes]\nsearch = " + search_select + ";")

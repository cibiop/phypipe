# PhyPipe: an automated pipeline for phylogenetic reconstruction from multilocus sequences

PhyPipe is a pipeline for performing phylogenetic analysis with probabilistic methods using DNA sequences from multiple loci.

It takes FASTA nucleotide files (one per locus) containing sequences for the taxa to be analyzed and outputs the phylogenetic tree reconstructed by the method of choice (Bayesian Inference or Maximum Likelihood).

PhyPipe's workflow covers sequence alignment, DNA evolution model selection for partitions, phylogenetic reconstruction and topological tests, using specific software tools for each step handling all intermediate configuration files. It works in Linux and Mac OS X systems.

PhyPipe implements RAxML and garli as software choices for Maximum Likelihood analysis. Also includes bootstrap topological tests.
MrBayes is the choice for performing Bayesian Inference analysis.

See [wiki page](https://gitlab.com/cibiop/phypipe/wikis/home) and [manual](Usermanual.pdf) for further information.


## *11/jul/2016* - PhyPipe was presented at the Bioinformatics Open Source Conference [(BOSC 2016)](https://www.open-bio.org/wiki/BOSC_2016)
Check our poster [**online**](http://f1000research.com/posters/5-1609).

## *6/ago/2016* - PhyPipe is now supported in Mac OS X systems!
Tested on Mac OS X El Capitan 10.11. Check the installation page in our [wiki](https://gitlab.com/cibiop/phypipe/wikis/home) or our [user manual](Usermanual.pdf) for further details.
///////////////////////////////

// paths to required software

python="/home/mgomezzul/phypipe-run/venv/bin/python"
dna2pep="/home/mgomezzul/MEGA/Documents/Projects/phypipe/scripts/apps/dna2pep/dna2pep.py"
revtrans="/home/mgomezzul/MEGA/Documents/Projects/phypipe/scripts/apps/revtrans/revtrans.py"
mafft="/home/mgomezzul/MEGA/Documents/Projects/phypipe/scripts/apps/mafft/bin/mafft"
partitionfinder="/home/mgomezzul/MEGA/Documents/Projects/phypipe/scripts/apps/partitionfinder/PartitionFinder.py"
mb="/home/mgomezzul/MEGA/Documents/Projects/phypipe/scripts/apps/mrbayes/bin/mb"
garli="/home/mgomezzul/MEGA/Documents/Projects/phypipe/scripts/apps/garli/bin/Garli"
sumtrees="/home/mgomezzul/phypipe/venv/bin/sumtrees.py"
raxmlHPC="/home/mgomezzul/MEGA/Documents/Projects/phypipe/scripts/apps/raxml/bin/raxmlHPC"
dna2pep="/home/mgomezzul/MEGA/Documents/Projects/phypipe/scripts/apps/dna2pep/dna2pep.py"
SCRIPTS="/home/mgomezzul/MEGA/Documents/Projects/phypipe/scripts"

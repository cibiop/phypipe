# Pipeline files description

This directory contains three files as follows:

* **dependencies.groovy:** This file must include system routes for software dependencies required by Phypipe.
* **pipeline.groovy:** This is the actual pipeline file that must be excecuted by bpipe for running the workflow.
* **ppipeline_stages.groovy:** This files contains stage definitions for each stage in the pipeline.
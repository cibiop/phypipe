//////////////////////////////////////////////////
//						//
//	            Phypipe                	//
//	Definition of the pipeline stages	//
//						//
//////////////////////////////////////////////////
// Actual pipeline in pipeline.groovy
// See manual for details on running
//
// Author: Nicolas D. Franco-Sierra <nfranco@eafit.edu.co>
// Last update: 17th Mar 2016

load "dependencies.groovy"

//////////////////////////////////////////////////
// execution parameters (modify them with the -p option)

// parameters for partitionfinder
branchlengths="1"	// default: linked 
method="1"		// default: mrbayes
modelselect="1"		// default: BIC
searchselect="1"	// default: greedy


// software choice for phylogenetic analysis
phylo_method = "mrbayes" // "mrbayes", "garli" or "raxml"

// parameters for MrBayes Block
ngen="10000" // very low value. just for testing purposes

// parameters for garli conf files
searchreps = "2"
bootstrapreps = "5"

// raxml run options
raxmlreps = "10" // very low value. just for testing purposes
raxmlmodel="GTRGAMMA"


//////// vars that you don't need to change /////
pffolder="partitionfinder/partition_finder.cfg"


/////////////////////////////////////////////////
// scripts

create_cfg="$SCRIPTS/create_cfg.py"
concat_fasta_align="$SCRIPTS/concat_fasta_align.py"
concat_fasta_alignments="$SCRIPTS/concat_fasta_alignments.py"
fasta2phy="$SCRIPTS/fasta2phy.py"
pfinder2mrbayesBlock="$SCRIPTS/pfinder2mrbayesBlock.py"
pfinder2garli="$SCRIPTS/pfinder2garli.py"
pfinder2raxml="$SCRIPTS/pfinder2raxml.py"
fasta2nexus="$SCRIPTS/fasta2nexus.py"

//////////////////////////////////////////////////
//						//
//						//
///////   pipeline stages definition	//////////

baseDir = file(".").absolutePath //working dir


check_run_info = {
       // stage for previous checks
       // not implemented yet
}


prepare_dir = {
    doc title: "Make work directories",
        desc:  """Creates directories for organizing files from all pipeline stages.""",
        constrains: " ",
        author: "nfranco@eafit.edu.co"
    exec "mkdir prot_seqs alignments alignments/prot alignments/dna partitionfinder phylo_analyses phylo_analyses/mrbayes phylo_analyses/garli phylo_analyses/raxml"
}

check_for_cds = {
    doc title: "Check input FASTA file status",
        desc:  """Looks for identifiers in input filenames in order to
                  redirect files properly according their status
                  (coding sequence '_cds.fasta', provided alignment '_al.fasta' and 
                  noncoding sequence).""",
        constrains: "FASTA files must end with '.fasta' extension",
        author: "nfranco@eafit.edu.co"
    def checkvar=input.split("_")[-1].split("\\.")[0]

    if(checkvar=="al"){
        branch.aligned=true
        branch.is_cds=false
    } else {
        branch.aligned=false
        if(checkvar[0..2]=="cds"){
            branch.is_cds=true
            if(checkvar.size()==3){
                branch.gcode="1"
            } else{
                branch.gcode=checkvar[3..-1]}
        } else{
            branch.is_cds=false}
      }
    forward input
    println "CDS status: $branch.is_cds for file: $branch"
}

translate = {
    doc title: "Translate coding sequences using DNA2PEP",
        desc:  """Translates protein coding sequences to amino acid sequences given 
                  the appropiate genetic code table (default: 1).""",
        constrains: """Nucleotide input sequence must be 'in-frame' (its length is 
                     divisible by three with no remainder).""",
        author: "nfranco@eafit.edu.co"
    if(branch.is_cds){
    output.dir="prot_seqs"
        filter("aa") {
            exec "$python $dna2pep -m $gcode -O fasta $input.fasta > $output.fasta"
        }
    } else {
        println "$branch.name treated as non coding sequence."
        forward input
        }
}

align = {
    doc title: "Align FASTA files using MAFFT",
        desc:  """This stage aligns FASTA files (nucleotide or amino acid sequences) if required
               according input file status.""",
        constrains: " ",
        author: "nfranco@eafit.edu.co"
    if(branch.is_cds){
        output.dir="alignments/prot"
        filter("alignment") {
	    exec "$mafft --quiet --auto  $input.fasta > $output.fasta"}
    } else if (branch.aligned) {
            println "$branch.name is already aligned. Skiping alignment step."
            output.dir="alignments/dna"
            filter("DNA.alignment") {
	        exec "cat $input.fasta > $output.fasta"
            }
    } else {
        output.dir="alignments/dna"
        filter("DNA.alignment") {
	    exec "$mafft --quiet --auto $input.fasta > $output.fasta"}
            }
}

reverse_translate = {
    doc title: "Convert amino acid alignment to nucleotides using RevTrans",
        desc:  """This stage transforms FASTA amino acid alignment file to 
               nucleotide alignment matrix.""",

        constrains: " ",
        author: "nfranco@eafit.edu.co"
    output.dir="alignments/dna"
    if(branch.is_cds){
        branch.prefix=branch.name+".fasta"
        transform('.aa.alignment.fasta') to('.DNA.alignment.fasta') {
	    exec "$python $revtrans $branch.prefix $input -mtx $gcode -O fasta $output"
            }
    } else {
        println "$branch.name treated as non coding sequence."
        forward input
        }
}

alignment =  segment{translate + align + reverse_translate}

concat = {
    doc title: "Concatenate nucleotide alignments to single FASTA file",
        desc:  """This stage concatenates FASTA alignments in a single file.
               It also generates a configuration file for partitionfinder.""",
        constrains: " ",
        author: "nfranco@eafit.edu.co"
    output.dir=baseDir
    exec "$python $create_cfg $pffolder alignments/dna/*.fasta $branchlengths $method $modelselect $searchselect"

    produce("concat_matrix.fasta") {
        exec "$python $concat_fasta_alignments $inputs $output"
    }
}

convert = {
    doc title: "Convert concatenated FASTA file to NEXUS and Phylip files",
        desc:  """This stage creates NEXUS and Phylip files to be used in 
               the downstream steps.""",
        constrains: " ",
        author: "nfranco@eafit.edu.co"
    exec "$python $fasta2phy $input partitionfinder/concat_matrix.phy"
    produce("concat_matrix.nexus") {
        from("fasta") {
            exec "$python $fasta2nexus $input $output"
            }
    }
}   

partition_finder = {
    doc title: "Find best partition scheme using partitionfinder",
        desc:  """This stage runs partitionfinder. It also creates MrBayes
               and garli blocks with best model information for each partition
               based on partitionfinder results.""",
        constrains: " ",
        author: "nfranco@eafit.edu.co"
    exec "$python $partitionfinder partitionfinder"
    exec "cd phylo_analyses/mrbayes && $python $pfinder2mrbayesBlock ../../$pffolder ../../partitionfinder/analysis/best_schemes.txt $ngen"
    exec "cd phylo_analyses/garli &&  $python $pfinder2garli ../../$pffolder ../../partitionfinder/analysis/best_schemes.txt $searchreps $bootstrapreps"
    exec "cd phylo_analyses/raxml && $python $pfinder2raxml ../../partitionfinder/analysis/best_schemes.txt"
    if (phylo_method=="raxml") {
        produce("concat_matrix.phy") {
            output.dir="phylo_analyses/raxml"
            exec "cp partitionfinder/concat_matrix.phy $output"
            }
    } else if (phylo_method=="mrbayes") {
            produce("concat_w_partitions.nexus") {
                output.dir="phylo_analyses/mrbayes"
                exec "cat $input phylo_analyses/mrbayes/Mrbayes_pfinder.txt > $output"
            }
    } else if (phylo_method=="garli") {
            produce("concat_w_partitions.nexus") {
                output.dir="phylo_analyses/garli"
                exec "cat $input phylo_analyses/garli/garli_pfinder.txt > $output"
            }
        }
}


phylo_analysis = {
    doc title: "Run phylogenetic analysis software",
        desc:  """This stage runs MrBayes or garli using files 
               generated through the whole pipeline. It outputs 
               consensus tree (for bayesian analysis) or best tree 
               with bootstrap support (for maximum likelihood analysis)""",
        constrains: " ",
        author: "nfranco@eafit.edu.co"
    if (phylo_method=="mrbayes") {
        output.dir="phylo_analyses/mrbayes"
        exec "$mb $input"
        produce("phylo_bayesian.cons.tre") {
            exec "mv phylo_analyses/mrbayes/concat_w_partitions.nexus.con.tre $output"
        }
    } else if (phylo_method=="garli") {
        output.dir="phylo_analyses/garli"
        exec "cd phylo_analyses/garli/ && $garli garli_ML.conf"
        exec "cd phylo_analyses/garli/ && $garli garli_BS.conf"
        exec "cd phylo_analyses/garli/ && $sumtrees -d0 -p -o results.tre -t concat_ML.best.tre concat_BS.boot.tre"
        produce("phylo_maxlikelihood.tree") {
            exec "mv phylo_analyses/garli/results.tre $output"
        }
    } else if (phylo_method=="raxml") {
        output.dir="phylo_analyses/raxml"
        exec "cd phylo_analyses/raxml/ && $raxmlHPC -f a -m $raxmlmodel -p 12345 -x 12345 -q parts.txt -# $raxmlreps -s concat_matrix.phy -n ML_tree"
        produce("phylo_raxML.tree") {
            exec "mv phylo_analyses/raxml/RAxML_bipartitions.ML_tree $output"
        }
    }
}

//////////////////////////////////////////////////
//						//
//	            Phypipe                	//
//	Phylogenetic analysis pipeline		//
//						//
//////////////////////////////////////////////////
//
// Author: Nicolas D. Franco-Sierra <nfranco@eafit.edu.co>
// Last update: 17th Mar 2016

// Run like:
// bpipe run -p @<parameter_file> [options] <path_to_this_file> <path_to_fasta_files>
// See documentation for further information
//

load "ppipeline_stages.groovy"

Bpipe.run { prepare_dir + "%.fasta" * [check_for_cds + alignment] + concat + convert + partition_finder + phylo_analysis}